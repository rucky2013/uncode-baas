package cn.uncode.baas.server.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.criteria.Model;
import cn.uncode.dal.utils.JsonUtils;
import cn.uncode.baas.server.constant.Resource;

public class O2oVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4463521142918762403L;
	
	//{"bucket":{"table":"restapp","field":"bucket","display":"name","order":"id"}
	//{"one":{"table":"restmethod","field":"id"},
	// "many":{"table":"restfield","field":"id","display":"name","order":"id"},
	// "join":{"table":"method2field","field":"restMethodId","field2":"restFieldId"}}
	private String table;
	private String field;
	private String field2;
	private String display;
	private String order;
	
	public static O2oVo valueOf(String value){
		if(StringUtils.isNotBlank(value)){
			Map<?, ?> tabMap = JsonUtils.fromJson(value, Map.class);
			if(tabMap != null){
				return O2oVo.valueOf(tabMap);
			}
		}
		return null;
	}
	
	public static O2oVo valueOf(Map<?,?> tabMap){
		if(tabMap != null){
			O2oVo vo = new O2oVo();
			if(tabMap.containsKey(Resource.REST_TABLE_MANY_TO_ONE_TABLE_KEY)){
				vo.setTable(String.valueOf(tabMap.get(Resource.REST_TABLE_MANY_TO_ONE_TABLE_KEY)));
			}
			if(tabMap.containsKey(Resource.REST_TABLE_MANY_TO_ONE_FIELD_KEY)){
				vo.setField(String.valueOf(tabMap.get(Resource.REST_TABLE_MANY_TO_ONE_FIELD_KEY)));
			}
			if(tabMap.containsKey(Resource.REST_TABLE_MANY_TO_ONE_DISPLAY_KEY)){
				vo.setDisplay(String.valueOf(tabMap.get(Resource.REST_TABLE_MANY_TO_ONE_DISPLAY_KEY)));
			}
			if(tabMap.containsKey(Resource.REST_TABLE_MANY_TO_ONE_ORDER_KEY)){
				vo.setOrder(String.valueOf(tabMap.get(Resource.REST_TABLE_MANY_TO_ONE_ORDER_KEY)));
			}
			if(tabMap.containsKey(Resource.REST_TABLE_MANY_TO_ONE_FIELD2_KEY)){
				vo.setField2(String.valueOf(tabMap.get(Resource.REST_TABLE_MANY_TO_ONE_FIELD2_KEY)));
			}
			return vo;
		
		}
		return null;
	}
	
	public Map<String, Object> getOrderMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Resource.REST_TABLE_MANY_TO_ONE_ORDER_KEY, order);
		map.put("pageSize", "1000");
		return map;
	}
	
	public List<String> getFields(){
		List<String> fds = new ArrayList<String>();
		fds.add(field);
		if(StringUtils.isNotEmpty(display)){
			fds.addAll(Arrays.asList(display.split(",")));
		}
		return fds;
	}
	
	/**
	 * build join model
	 * @param database
	 * @param field
	 * @param field2
	 * @return
	 */
	public Model getJoinModel(String database, Object field, Object field2){
		if(StringUtils.isNotBlank(table)){
			Model model = new Model(table);
			model.setDatabase(database);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(this.field, field);
			params.put(this.field2, field2);
			model.addContent(params);
			return model;
		}
		return null;
	}
	
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	

}
