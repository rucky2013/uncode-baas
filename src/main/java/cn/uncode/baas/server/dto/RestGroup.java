package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class RestGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String NAME = "name";
	public static final String DESC = "desc";
	public static final String ROLES = "roles";
	
	private int id;
	private String bucket;
	private String name;
	private String desc;
	private String roles;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getRoles() {
		return roles;
	}
	
	public void setRoles(String roles) {
		this.roles = roles;
	}
	
	public void addRole(String role){
		if(StringUtils.isNotBlank(this.roles)){
			this.roles = role;
		}else{
			this.roles += "," + role;
		}
	}
	
	public void removeRole(String role){
		if(this.roles.endsWith(role)){
			this.roles = this.roles.replace(","+role, "");
		}else{
			this.roles = this.roles.replace(role+",", "");
		}
	}
	
	public static RestGroup valueOf(Map<String, Object> map){
		RestGroup restGroup = null;
		if(null != map){
			restGroup = new RestGroup();
			if(map.containsKey(ID)){
				restGroup.setId((Integer)map.get(ID));
			}
			if(map.containsKey(BUCKET)){
				restGroup.setBucket(String.valueOf(map.get(BUCKET)));
			}
			if(map.containsKey(NAME)){
				restGroup.setName(String.valueOf(map.get(NAME)));
			}
			if(map.containsKey(DESC)){
				restGroup.setDesc(String.valueOf(map.get(DESC)));
			}
			if(map.containsKey(ROLES)){
				restGroup.setRoles(String.valueOf(map.get(ROLES)));
			}
		}
		return restGroup;
	}
	

}
