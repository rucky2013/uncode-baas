package cn.uncode.baas.server.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.uncode.baas.server.constant.Resource;
import cn.uncode.baas.server.internal.context.RestContextManager;

public class AccessTokenInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = Logger.getLogger(AccessTokenInterceptor.class);

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	// access token
    	String accessToken = request.getHeader(Resource.REQ_ACCESS_TOKEN);
    	if(StringUtils.isNotEmpty(accessToken)){
    		RestContextManager.initAccessToken(accessToken);
		}
        return true;
    }
}
