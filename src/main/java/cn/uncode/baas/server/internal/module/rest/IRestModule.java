package cn.uncode.baas.server.internal.module.rest;

import java.util.Map;

import cn.uncode.baas.server.exception.MethodNotFoundException;
import cn.uncode.baas.server.exception.ValidateException;

import javax.script.ScriptException;


public interface IRestModule {
	
	public static final String REST_METHOD = "method";
	public static final String REST_PARAMS = "params";
	public static final String REST_METHOD_PLUGIN_NAME = "plugin";
	
	
	/**
	 * bucket内调用
	 * @param param
	 * @return
	 * @throws ScriptException
	 * @throws NoSuchMethodException
	 * @throws ValidateException
	 * @throws MethodNotFoundException
	 */
	Object invoke(Object param) throws ScriptException, NoSuchMethodException, ValidateException, MethodNotFoundException;
	
	Map<String, Object> get(String url);
	
	Map<String, Object> get(String url, Object param);
		
	Map<String, Object> post(String url, Object param);
		
	void put(String url, Object param);

	void delete(String url, Object param);

}
