package cn.uncode.baas.server.internal.module.acl;

public interface IAclModule {
	
	void saveRole(Object param);
	
	void removeRole(String name);
	
	void saveGroup(Object param);
	
	void removeGroup(String name);
	
	void userAuth(Object param);
	
	void removeUserAuth(String username);

}
